import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {PeliculaService} from '../../services/pelicula.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
  providers: [PeliculaService]
})
export class DetalleComponent implements OnInit {

	public pelicula: [];

  constructor(
		private _route: ActivatedRoute,
  	private _router: Router,
  	public _peliculaService: PeliculaService
  ) {}

  ngOnInit() {

  	this._route.params.subscribe((params: Params) => {

  		this._peliculaService.getPelicula(params.id).subscribe(
	  		response => {
	  			console.log(response);
	  			if(response)
	  			{
	  				this.pelicula = response;
	  			}
	  			else{}
	  		},
	  		error => {
	  			console.log(error);
	  		}
	  	);
  	});
  }
  volver() {
  	this._router.navigate(['/peliculas']);
  }
}
