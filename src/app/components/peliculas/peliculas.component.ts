import { Component, OnInit, Input } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {PeliculaService} from '../../services/pelicula.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.scss'],
  providers: [PeliculaService]
})
export class PeliculasComponent implements OnInit {
  
  public titulo:string;
  public peliculas: [];

  constructor(
  	private _route: ActivatedRoute,
  	private _router: Router,
  	public _peliculaService: PeliculaService
  ) {
  	this.titulo = "Últimos estrenos";
  }

  ngOnInit() {
  	this._peliculaService.getPeliculas().subscribe(
  		response => {
  			console.log(response);
  			if(response)
  			{
  				this.peliculas = response;
  			}
  			else{}
  		},
  		error => {
  			console.log(error);
  		}
  	);
  }
}