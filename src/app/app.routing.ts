import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {PeliculasComponent} from './components/peliculas/peliculas.component';
import {DetalleComponent} from './components/detalle/detalle.component';
import {ErrorComponent} from './components/error/error.component';

const appRoutes: Routes = [
		{path: '', component: PeliculasComponent},
		{path: 'peliculas', component: PeliculasComponent},
		{path: 'detalle/:id', component: DetalleComponent},
		{path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);