import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Global} from './global';
import {map} from "rxjs/operators";
import {Router} from '@angular/router';

@Injectable()

export class PeliculaService {

	public url: string;
	public apiKey: string;
	public urlDetalle: string;

	constructor(
		private _http: HttpClient,
		private _router: Router
	) {
		this.url = Global.url;
		this.apiKey = Global.apiKey;
		this.urlDetalle = Global.urlDetalle;
	}

	getPeliculas():Observable<any> {
		return this._http.get(this.url+'?api_key='+this.apiKey+'&page=1').pipe(map((data:any)=> data.results));		
	}

	getPelicula(id:string):Observable<any> {
		return this._http.get(this.urlDetalle+id+'?api_key='+this.apiKey).pipe(map((data:any)=> data));
	}

	showPelicula(id) {
		this._router.navigate(['/detalle/'+id]);
	}
}